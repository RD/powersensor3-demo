# PowerSensor3 Demo

This repositroy contains backround materials to the PowerSensor3 demonstrator.

## Why did we develop the PowerSensor3?
If you are able to measure the power utilization of your device while your application is executed you can answer questions such as:
- How much power does my application use?
- Can I trade time to solution for power utilization (e.g. lower clock rate)?
- We promised our customer / our funder to reduce power, how to show this?
- Can I change my algorithm to use less power?
- I see weird behaviour, what is going on?

Many of todays CPU's, GPU's, FPGA cards and embedded devices have support for power measurements through an open iterface or vendor library. For many uses this is sufficient, however there are also several shortcomings:
- Often the accuracy (both in range as well as time resolution) is unknown
- Resolution (time) is lacking
- Sensor readings might not be synchronized with code at low enough level of detail
- Your device might not have an onboard sensor

We previously designed the [PowerSensor2](https://www.astron.nl/~romein/papers/ISPASS-18/paper.pdf) to measure the power utilization of PCIe devices such as GPUs. With this sensor we were able to identify the power utilization of individual GPU kernels which provides valuabla insights. However, the PowerSensor2 had some shortcomings which we adressed with the design of PowerSensor3.

## This demonstrator
The PowerSensor3 demonstrator consists of several different components. In this demonstrator these components are stacked and used together, the components might also be used stand-alone. The below image illustrates the Hardware + Firmware + Software stack of this demonstrator.

![Hardware - Firmware - Software Stack of the PowerSensor3 demonstrator](PowerSensor3Demo-HW-FW-SW-stack.png)

### [PowerSensor3 hardware](https://git.astron.nl/RD/powersensor3)
The PowerSensor3 is an openly usable hardware design with a baseboard, hosting an Arduino compatible STM module along with open designed sensor boards. The sensor board base desing can be configured to match certain voltage and current ranges but might also be modified at your need.

### [PowerSensor3 Firmware and host libraries](https://github.com/nlesc-recruit/PowerSensor3)
The PowerSensor3 comes with open firmware for the STM module along with host libraries to configure and use the PowerSensor3. The PowerSensor3 is connected through USB to a host system. The sensor can either be used as stand alone, to measure power utilization while running an application or might be intergrated more tightly with your application through the C++ libraries.

### [Power Measurement Toolkit (PMT)](https://git.astron.nl/RD/pmt)
The Power Measurement Toolkit ([PMT](https://arxiv.org/pdf/2210.03724.pdf)) is a tool that supports and can combine multiple sensors, either from the host hardware (RAPL, NVML, Tegra, AMD Alveo Accelerators, etc.). or from external devices such as the PowerSensor2 and PowerSensor3.

### [PMT Viewer](https://git.astron.nl/RD/pmt-view)
PMT viewer is a web based visualisation tool on top of the Power Measurement Toolkit. It is the visualisation used in this demonstrator.

## PowerSensor3 repositories
- [PowerSensor3 hardware](https://git.astron.nl/RD/powersensor3)
- [PowerSensor3 Firmware and host libraries](https://github.com/nlesc-recruit/PowerSensor3)
- [Power Measurement Toolkit (PMT)](https://git.astron.nl/RD/pmt)
- [PMT Viewer (this demo)](https://git.astron.nl/RD/pmt-view)
- [Kernel Tuner (can use power sensors through PMT)](https://github.com/KernelTuner)
- [Kernel Tuner PowerSensor3 demo](https://github.com/nlesc-recruit/powersensor_demo)

## Background
- [PowerSensor2 publication](https://www.astron.nl/~romein/papers/ISPASS-18/paper.pdf)
- [Power Measurement Toolkit publication](https://arxiv.org/pdf/2210.03724.pdf)
- [Presentation on PowerSensor3](https://casper.astro.berkeley.edu/workshop2023/agenda/presentations/day3/11_SV.pdf)

## Contact
Steven van der Vlugt: vlugt@astron.nl

## License
Licensed under Apache 2.0, see License file


